# Running `CAVM` in real-world C program.

Basically, `CAVM` compiles target C source file in to shared library, then invokes target function from shared library to execute it. However if target C source file requires extra settings to compile, it is hard for `CAVM` to build shared library.

The real-world C program consists of bunch of C files and often uses build automation tool such as `make` or `cmake`. Each C file often requires additional information(such as defined macro, path, and libraries) to compile. These additional information is controlled by compilation flags in building process. By providing required compilation flags to `CAVM`, `CAVM` can instrument and run function.

## zlib
Although whole project consists of several C files and is managed by `make`, compilation flag on target C file can be simple.
```sh
...
gcc -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/adler32.o adler32.c
gcc -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/crc32.o crc32.c
gcc -O3 -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC -c -o objs/deflate.o deflate.c
...
```
Seeing the commands executed during `make` of `zlib`, We can observe that several defined macros are only needed to compile `crc32.c`. By providing the macro define compilation flags to `CAVM`, `CAVM` can generate test input automatically.

For example, following steps describe how to generate test input generation for function `crc32_z` in `crc_32.c`.

1. Get source code and build `zlib` normally.

2. Extract compilation flags from commands from `make`.
    
    `make` executes aforementioned commands in order to compile `crc_32.c`.  
    From the commands, compilation flags of `crc32.c`, `-fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC` can be extracted.
    
3. Run cavm with extracted compilation flags.
    
    ```sh
    $ cavm run zlib-1.2.11/crc32.c -f crc32_z --flags -fPIC -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN  -DPIC
    {
      "1": {
        "false": {
          "vector": [
            79,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                24
              ]
            },
            38
          ],
          "counter": 0
        },
        "true": {
          "vector": [
            26,
            {
              "underlying_type": "unsigned char",
              "pointee": null
            },
            23
          ],
          "counter": 1
        }
      },
      "2": {
        "false": {
          "vector": [
            25,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                19,
                47,
                72,
                34
              ]
            },
            45
          ],
          "counter": 37
        },
        "true": {
          "vector": [
            20,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                8,
                59
              ]
            },
            71
          ],
          "counter": 15
        }
      },
      "3": {
        "false": {
          "vector": [
            72,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                29
              ]
            },
            91
          ],
          "counter": 4
        },
        "true": {
          "vector": [
            45,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                49
              ]
            },
            28
          ],
          "counter": 4
        }
      },
      "4": {
        "false": {
          "vector": [
            79,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                24
              ]
            },
            38
          ],
          "counter": 7
        },
        "true": {
          "vector": [
            71,
            {
              "underlying_type": "unsigned char",
              "pointee": [
                92
              ]
            },
            46
          ],
          "counter": 4
        }
      },
      "report": {
        "coveredBranches": 8,
        "totalEvals": 72,
        "totalBranches": 8
      }
    }
    ```


## GnuMP

`mpz_abs` function in `mpz/abs.c` of `GnuMP` requires external path or libraries to compile. When the target source file requires external path or libraries, it is hard for `CAVM` to run it with All-In-One command.

Instead, `CAVM` separated `instrumentation` part and `search` part. Building is not handled by `CAVM`, original building process of target program(`GnuMP`) will be used.

1. In order to capture compilation flags, build `GnuMP` normally.
    ```sh
    $ ./configure
    $ make
    ```
    
2. Extract compilation flags from `make`
    
    Observing the command line,
    ```sh
    ...
    /bin/bash ../libtool  --tag=CC   --mode=compile gcc -DHAVE_CONFIG_H -I. -I..  -D__GMP_WITHIN_GMP -I..   -O2 -pedantic -fomit-frame-pointer -m64 -mtune=broadwell -march=broadwell -c -o abs.lo abs.c
    libtool: compile:  gcc -DHAVE_CONFIG_H -I. -I.. -D__GMP_WITHIN_GMP -I.. -O2 -pedantic -fomit-frame-pointer -m64 -mtune=broadwell -march=broadwell -c abs.c  -fPIC -DPIC -o .libs/abs.o
    libtool: compile:  gcc -DHAVE_CONFIG_H -I. -I.. -D__GMP_WITHIN_GMP -I.. -O2 -pedantic -fomit-frame-pointer -m64 -mtune=broadwell -march=broadwell -c abs.c -o abs.o >/dev/null 2>&1
    ...
    ```
    We can obtain compilation flags `-DHAVE_CONFIG_H -I. -I.. -D__GMP_WITHIN_GMP -I.. -O2 -pedantic -fomit-frame-pointer -m64 -mtune=broadwell -march=broadwell -fPIC -DPIC`
    
3. Find internal function name.
    
    One of the characteristic of `GnuMP` is that they defined macros so that function name are changed internally during pre-processing.  
    In `gmp.h`, we can see that target function `mpz_abs` are changed to `__gmpz_abs` internally.
    ```c
    ...  
    #define mpz_abs __gmpz_abs
    #if __GMP_INLINE_PROTOTYPES || defined (__GMP_FORCE_mpz_abs)
    __GMP_DECLSPEC void mpz_abs (mpz_ptr, mpz_srcptr);
    #endif
    ...
    ```
    To designate `mpz_abs` as target function, we have to use `__gmpz_abs` instead of `mpz_abs`.
    
4. designate struct name.
    
    Currently, `CAVM` needs struct name to get the definition of the target function's input struct.  However, `GnuMP` does not designated the struct name for each struct. In `gmp.h`,
    ```c
    typedef struct
    {
      int _mp_alloc;
      int _mp_size;
      mp_limb_t *_mp_d;
    } __mpz_struct;
    ```
    For `CAVM` to get the definition of `__mpz_struct`, assign struct name to it as seen below.
    ```c
    typedef struct __mpz_struct
    {
      int _mp_alloc;
      int _mp_size;
      mp_limb_t *_mp_d;
    } __mpz_struct;
    ```
    
5. Instrument the target file with obtained compilation flags.
    
    ```sh
    $ cd mpz
    $ cavm instrument ./abs.c -f __gmpz_abs --flags -DHAVE_CONFIG_H -I. -I.. -D__GMP_WITHIN_GMP -I.. -O2 -pedantic -fomit-frame-pointer  
    $ mv abs.c abs.bak.c
    $ mv abs.inst.c abs.c
    ```
    
6. Run `make` of `GnuMP`
    
    Return to top-level directory of `GnuMP` and build it with original Makefile
    ```sh
    $ cd ..
    $ make
    ```
    
7. Run `CAVM Search`
    
    ```sh
    $ cd mpz
    $ cavm search ./abs.c ../.libs/libgmp.so -f __gmpz_abs --flags -DHAVE_CONFIG_H -I. -I.. -D__GMP_WITHIN_GMP -I..
    {
    "1": {
      "false": {
        "counter": 1000,
        "vector": null
      },
      "true": {
        "counter": 0,
        "vector": [
          {
            "underlying_type": "struct __mpz_struct",
            "pointee": {
              "is_recursive": false,
              "members": [
                5,
                17,
                {
                  "underlying_type": "unsigned long",
                  "pointee": [
                    2,
                    21,
                    14,
                    85,
                    9,
                    38,
                    48,
                    69,
                    30,
                    5,
                    57,
                    60,
                    78,
                    76,
                    36
                  ]
                }
              ],
              "name": "struct __mpz_struct",
              "decl": [
                "struct __mpz_struct{int _mp_alloc; int _mp_size; unsigned long * _mp_d; };",
                [
                  "int",
                  "int",
                  "unsigned long *"
                ]
              ]
            }
          },
          {
            "underlying_type": "struct __mpz_struct",
            "pointee": {
              "is_recursive": false,
              "members": [
                1,
                0,
                {
                  "underlying_type": "unsigned long",
                  "pointee": [
                    57,
                    88,
                    65,
                    35,
                    1,
                    51,
                    9,
                    27,
                    47,
                    80,
                    37,
                    82
                  ]
                }
              ],
              "name": "struct __mpz_struct",
              "decl": [
                "struct __mpz_struct{int _mp_alloc; int _mp_size; unsigned long * _mp_d; };",
                [
                  "int",
                  "int",
                  "unsigned long *"
                ]
              ]
            }
          }
        ]
      }
    },
    "6": {
      "false": {
        "counter": 0,
        "vector": [
          {
            "underlying_type": "struct __mpz_struct",
            "pointee": {
              "is_recursive": false,
              "members": [
                5,
                17,
                {
                  "underlying_type": "unsigned long",
                  "pointee": [
                    2,
                    21,
                    14,
                    85,
                    9,
                    38,
                    48,
                    69,
                    30,
                    5,
                    57,
                    60,
                    78,
                    76,
                    36
                  ]
                }
              ],
              "name": "struct __mpz_struct",
              "decl": [
                "struct __mpz_struct{int _mp_alloc; int _mp_size; unsigned long * _mp_d; };",
                [
                  "int",
                  "int",
                  "unsigned long *"
                ]
              ]
            }
          },
          {
            "underlying_type": "struct __mpz_struct",
            "pointee": {
              "is_recursive": false,
              "members": [
                1,
                0,
                {
                  "underlying_type": "unsigned long",
                  "pointee": [
                    57,
                    88,
                    65,
                    35,
                    1,
                    51,
                    9,
                    27,
                    47,
                    80,
                    37,
                    82
                  ]
                }
              ],
              "name": "struct __mpz_struct",
              "decl": [
                "struct __mpz_struct{int _mp_alloc; int _mp_size; unsigned long * _mp_d; };",
                [
                  "int",
                  "int",
                  "unsigned long *"
                ]
              ]
            }
          }
        ]
      }
    },
    "report": {
      "coveredBranches": 2,
      "totalEvals": 1000,
      "totalBranches": 3
    }
    }
    ```


## Busybox

As we discussed before, target function is executed via shared library. That is, the target program must be compiled into shared library and target function must be reachable from shared library.

Below step describe test input generation for function `fgcolor`.

1. Configure busybox so that it make shared library.
    
    ```sh
    $ make defconfig
    $ make menuconfig
    ```
    Select Busybox Settings -> Build shared libbusybox
    
2. Build busybox and capture flags for ls.c
    ```sh
    $ make V=1
    ```
    
3. CAVM instrument
    
    ```sh
    $ cavm instrument coreutils/ls.c -f fgcolor --flags -std=gnu99 -Iinclude -Ilibbb  -include include/autoconf.h -D_GNU_SOURCE -DNDEBUG -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D"BB_VER=KBUILD_STR(1.27.2)" -DBB_BT=AUTOCONF_TIMESTAMP  -Wall -Wshadow -Wwrite-strings -Wundef -Wstrict-prototypes -Wunused -Wunused-parameter -Wunused-function -Wunused-value -Wmissing-prototypes -Wmissing-declarations -Wno-format-security -Wdeclaration-after-statement -Wold-style-definition -fno-builtin-strlen -finline-limit=0 -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-guess-branch-probability -funsigned-char -static-libgcc -falign-functions=1 -falign-jumps=1 -falign-labels=1 -falign-loops=1 -fno-unwind-tables -fno-asynchronous-unwind-tables -fno-builtin-printf -Os -fpic -fvisibility=hidden     -D"KBUILD_STR(s)=#s" -D"KBUILD_BASENAME=KBUILD_STR(ls)"  -D"KBUILD_MODNAME=KBUILD_STR(ls)"
    
    $ mv coreutils/ls.c coreutils/ls.bak.c
    $ mv coreutils/ls.inst.c coreutils/ls.c
    ```
    
4. modify it to make target function visible via shared library.
    1. Open `coreutils/ls.c` and remove `static` keyword in fgcolor defintion.
    
    2. Open `include/libbb.h` and add function prototype and busybox's `EXTERNALLY_VISIBLE` macro.
        
        ```c
        char fgcolor(mode_t mode) EXTERNALLY_VISIBLE;
        ```
        take care for the inserting location, we inserted above commands after ls_main declaration,
        ```c
        int ls_main(int argc, char **argv) IF_LS(MAIN_EXTERNALLY_VISIBLE);
        ```
        because our target file is `ls`
    
    3. move `trace` and `get_trace()` definition from `coreutils/branch_distance.h` to `include/libbb.h` to make it visible.
        
        ```c
          typedef struct _trace {
            int stmtid;
            int result;
            double true_distance;
            double false_distance;
            struct _trace *next;
          } trace;
          trace get_trace(void ) EXTERNALLY_VISIBLE;
        ```
        We inserted this after above insertion.
    
5. Build busybox
    
    ```sh
    $ make
    ```
    
6. CAVM search
    
    ```sh
    $ cavm search coreutils/ls.c 0_lib/libbusybox.so.1.27.2_unstripped -f fgcolor --max 1000000000000000000000 --flags -std=gnu99 -Iinclude -Ilibbb  -include include/autoconf.h -D_GNU_SOURCE -DNDEBUG -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D"BB_VER=KBUILD_STR(1.27.2)" -DBB_BT=AUTOCONF_TIMESTAMP  -Wall -Wshadow -Wwrite-strings -Wundef -Wstrict-prototypes -Wunused -Wunused-parameter -Wunused-function -Wunused-value -Wmissing-prototypes -Wmissing-declarations -Wno-format-security -Wdeclaration-after-statement -Wold-style-definition -fno-builtin-strlen -finline-limit=0 -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-guess-branch-probability -funsigned-char -static-libgcc -falign-functions=1 -falign-jumps=1 -falign-labels=1 -falign-loops=1 -fno-unwind-tables -fno-asynchronous-unwind-tables -fno-builtin-printf -Os -fpic -fvisibility=hidden     -D"KBUILD_STR(s)=#s" -D"KBUILD_BASENAME=KBUILD_STR(ls)"  -D"KBUILD_MODNAME=KBUILD_STR(ls)"
    {
      "1": {
        "false": {
          "counter": 1,
          "vector": [
            1467237915
          ]
        },
        "true": {
          "counter": 10,
          "vector": [
            369855880
          ]
        }
      },
      "report": {
        "totalBranches": 2,
        "totalEvals": 11,
        "coveredBranches": 2
      }
    }
    ```
