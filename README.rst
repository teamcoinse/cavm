Coinse AVM (CAVM)
=================

CAVM (read ``kaboom`` \|kəˈbuːm\|) is a test data generation tool for
C/C++ code, based on the Alternating Variable Method (AVM).

Dependencies
------------

This project is using `clang
3.9 <http://llvm.org/releases/download.html#3.9.0>`__. You can download
pre-built binaries and build script will refer it.

Usage
-----
- `Quick Start`_
- `Examples with real world C projects <./instruction.md>`__.

.. _Quick Start: #Build

Build
-----

    This project is based on
    (https://github.com/eliben/llvm-clang-samples) and
    (https://github.com/eliben/llvm-clang-samples/pull/11) especially
    for MacOS compatibility.

| First, you need to set the path of pre-built binaries of clang.
|  Following is the command to build and install ``cavm`` python package
  which instruments and analyzes target code.

.. code:: sh

    $ export CLANG_DIR=./clang+llvm-3.9.0-x86_64-apple-darwin
    $ make
    $ make test

If you have built llvm and clang using source code at your machine, you
may use it directly. In this case, please look at ``Makefile``.

Run
---

``cavm`` returns result in JSON format. You can get input vector by ``result['branch_id']['condition']['vector']``.

.. code:: sh

    $ python3 -m cavm.main run sample/case1.c -f case1
    {
      "2": {
        "false": {
          "vector": [
            43,
            6,
            88
          ],
          "counter": 22
        },
        "true": {
          "vector": [
            96,
            100,
            14
          ],
          "counter": 79
        }
      },
      "1": {
        "true": {
          "vector": [
            43,
            6,
            88
          ],
          "counter": 0
        },
        "false": {
          "vector": [
            100,
            -73,
            95
          ],
          "counter": 1
        }
      },
      "report": {
        "totalEvals": 102,
        "coveredBranches": 4,
        "totalBranches": 4
      }
    }

You can start with examples in ``/sample``. CAVM takes path of target
code and name of target function as command line arguments. For full
usage instructions, please see the output of:

.. code:: sh

    $ python3 -m cavm.main --help

Structure
---------

Main part of CAVM is built with python. We use clang to instrument
target C/C++ code and it provides python interface. Following is the
short description of structure of project:

-  ``cavm/``: python codes of core CAVM logic.

-  ``lib/``: clang related codes.

-  ``sample/`` : directory of sample test source code files

-  ``Makefile`` : Makefile to compile clang package and standalone
   instrumentation tool.

Instrumenation
--------------

``cavm`` python package provides interface to use clang from python. You
can build and install ``cavm`` package:

.. code:: sh

    $ make

.. code:: sh

    $ python3 -m cavm.main instrument sample/case1.c -f case1
    sample/case1.inst.c is created.

You can just instrument the target code without searching.
It will generate following files in the same directory of target code. 

-  ``<filename>_first_round.c``: ``switch-case`` unrolled version code.

-  ``<filename>.inst.c``: intrumented code.


Distribution
------------

Making python wheel distibution (linux and OSx are only supported).

.. code:: sh

    $ make wheel
