"""
Copyright (C) 2017 by Junhwi Kim <junhwi.kim23@gmail.com>
Copyright (C) 2017 by Byeonghyeon You <byou@kaist.ac.kr>
Copyright (C) 2017 by Gabin An <agb94@kaist.ac.kr>

Licensed under the MIT License:
See the LICENSE file at the top-level directory of this distribution.
"""

import unittest
import subprocess
import json


def run_cavm(popen_cmd):
    cmd = ['python3', '-m', 'cavm.main', 'run'] + popen_cmd
    proc = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc_stdout, _ = proc.communicate()
    return proc.returncode, proc_stdout


class TestCAVMCheck(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        subprocess.run(['make', 'python'])

    def assertion(self, result, branches):
        self.assertEqual(len(result.keys()), branches + 1)
        for k in result:
            if k == 'report':
                continue
            self.assertNotEqual(result[k]['true']['vector'], None, (k, result))
            self.assertNotEqual(result[k]['false']['vector'], None, (k, result))

    def test_linkedlist_insert(self):
        cmd = ['sample/linkedlist.c', '-f', 'insert']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 4)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0]['pointee'], None, (1, c_input))
            else:
                self.assertEqual(c_input[0]['pointee'], None, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def test_branch_2(c_input, condition):
            test_branch_1(c_input, False)
            if condition == False:
                self.assertIn(c_input[2], range(1, c_input[0]['pointee']['members'][0] + 2), (2, c_input))
            else:
                self.assertNotIn(c_input[2], range(1, c_input[0]['pointee']['members'][0] + 2), (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

        def test_branch_3(c_input, condition):
            test_branch_1(c_input, False)
            test_branch_2(c_input, False)
            if condition == False:
                self.assertNotEqual(c_input[2], 1, (3, c_input))
            else:
                self.assertEqual(c_input[2], 1, (3, c_input))

        test_branch_3(result['3']['false']['vector'], False)
        test_branch_3(result['3']['true']['vector'], True)

    def test_linkedlist_modify(self):
        cmd = ['sample/linkedlist.c', '-f', 'modify']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 2)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertIn(c_input[2], range(1, c_input[0]['pointee']['members'][0] + 1), (1, c_input))
            else:
                self.assertNotIn(c_input[2], range(1, c_input[0]['pointee']['members'][0] + 1), (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_linkedlist_delete(self):
        cmd = ['sample/linkedlist.c', '-f', 'delete']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 3)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertIn(c_input[1], range(1, c_input[0]['pointee']['members'][0] + 2), (1, c_input))
            else:
                self.assertNotIn(c_input[1], range(1, c_input[0]['pointee']['members'][0] + 2), (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def test_branch_2(c_input, condition):
            test_branch_1(c_input, False)
            if condition == False:
                self.assertNotEqual(c_input[1], 1, (2, c_input))
            else:
                self.assertEqual(c_input[1], 1, (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

    def test_triangle(self):
        cmd = ['sample/triangle.c', '-f', 'get_type', '--min', '1']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertEqual(len(result.keys()), 8 + 1)
        for k in result:
            if k == 'report':
                continue
            if k == '7':
                self.assertEqual(result[k]['true']['vector'], None)
            else:
                self.assertNotEqual(result[k]['true']['vector'], None)
            self.assertNotEqual(result[k]['false']['vector'], None)

        def test_range(c_input):
            self.assertGreaterEqual(c_input[0], 1, c_input)
            self.assertGreaterEqual(c_input[1], 1, c_input)
            self.assertGreaterEqual(c_input[2], 1, c_input)

        def test_branch_1(c_input, condition):
            test_range(c_input)
            if condition == False:
                self.assertLessEqual(c_input[0], c_input[1], (1, c_input))
            else:
                self.assertGreater(c_input[0], c_input[1], (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def test_branch_2(c_input, condition):
            test_range(c_input)
            c_input[0], c_input[1] = min(c_input[0], c_input[1]), max(
                c_input[0], c_input[1])
            if condition == False:
                self.assertLessEqual(c_input[0], c_input[2], (2, c_input))
            else:
                self.assertGreater(c_input[0], c_input[2], (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

        def test_branch_3(c_input, condition):
            test_range(c_input)
            c_input = [
                min(min(c_input[0], c_input[1]), c_input[2]), max(
                    c_input[0], c_input[1]), max(
                        min(c_input[0], c_input[1]), c_input[2])
            ]
            if condition == False:
                self.assertLessEqual(c_input[1], c_input[2], (3, c_input))
            else:
                self.assertGreater(c_input[1], c_input[2], (3, c_input))

        test_branch_3(result['3']['false']['vector'], False)
        test_branch_3(result['3']['true']['vector'], True)

        def test_branch_4(c_input, condition):
            test_range(c_input)
            if condition == False:
                self.assertGreater(c_input[0] + c_input[1], c_input[2],
                                   (4, c_input))
            else:
                self.assertLessEqual(c_input[0] + c_input[1], c_input[2],
                                     (4, c_input))

        test_branch_4(sorted(result['4']['false']['vector']), False)
        test_branch_4(sorted(result['4']['true']['vector']), True)

        def test_branch_5(c_input, condition):
            test_branch_4(c_input, False)
            if condition == False:
                self.assertNotEqual(c_input[0], c_input[1], (5, c_input))
            else:
                self.assertEqual(c_input[0], c_input[1], (5, c_input))

        test_branch_5(sorted(result['5']['false']['vector']), False)
        test_branch_5(sorted(result['5']['true']['vector']), True)

        def test_branch_6(c_input, condition):
            test_branch_5(c_input, True)
            if condition == False:
                self.assertNotEqual(c_input[1], c_input[2], (6, c_input))
            else:
                self.assertEqual(c_input[1], c_input[2], (6, c_input))

        test_branch_6(sorted(result['6']['false']['vector']), False)
        test_branch_6(sorted(result['6']['true']['vector']), True)

        def test_branch_7(c_input, condition):
            if condition == False:
                test_branch_5(c_input, False)
                self.assertNotEqual(c_input[0], c_input[1], (7, c_input))
            else:
                self.assertEqual(c_input, None, (7, c_input))

        test_branch_7(sorted(result['7']['false']['vector']), False)
        test_branch_7(result['7']['true']['vector'], True)

        def test_branch_8(c_input, condition):
            test_branch_7(c_input, False)
            if condition == False:
                self.assertNotEqual(c_input[1], c_input[2], (8, c_input))
            else:
                self.assertEqual(c_input[1], c_input[2], (8, c_input))

        test_branch_8(sorted(result['8']['false']['vector']), False)
        test_branch_8(sorted(result['8']['true']['vector']), True)

    def test_unary(self):
        cmd = ['sample/unary.c', '-f', 'positive']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 3)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertGreaterEqual(c_input[0], -10, (1, c_input))
            else:
                self.assertLess(c_input[0], -10, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def test_branch_2(c_input, condition):
            test_branch_1(c_input, False)
            if condition == False:
                self.assertLessEqual(c_input[0], 0, (2, c_input))
            else:
                self.assertGreater(c_input[0], 0, (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

        def test_branch_3(c_input, condition):
            test_branch_2(c_input, True)
            if condition == False:
                self.assertNotEqual(c_input[0], 8, (3, c_input))
            else:
                self.assertEqual(c_input[0], 8, (3, c_input))

        test_branch_3(result['3']['false']['vector'], False)
        test_branch_3(result['3']['true']['vector'], True)

    def test_calendar(self):
        cmd = ['sample/calendar.c', '-f', 'daysBetween']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 23)

        def is_leap_year(year):
            return (year % 4 == 0 and year % 100 != 0) or year % 400 == 0

        def month_days(month, year):
            days = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
            return 29 if month == 2 and is_leap_year(year) else days[month - 1]

        def is_swap_needed(c_input):
            start = [c_input[2], c_input[0], c_input[1]]
            end = [c_input[5], c_input[3], c_input[4]]
            return start > end

        def sanitize_month(c_input):
            c_input[0] = min(max(c_input[0], 1), 12)
            c_input[3] = min(max(c_input[3], 1), 12)
            return c_input

        def sanitize_days(c_input):
            c_input[1] = min(
                max(c_input[1], 1), month_days(c_input[0], c_input[2]))
            c_input[4] = min(
                max(c_input[4], 1), month_days(c_input[3], c_input[5]))
            return c_input

        def sanitize(c_input):
            return sanitize_days(sanitize_month(c_input))

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertGreaterEqual(c_input[0], 1, (1, c_input))
            else:
                self.assertLess(c_input[0], 1, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def swap_dates(c_input):
            c_input = sanitize(c_input)
            if is_swap_needed(c_input):
                return [
                    c_input[3], c_input[4], c_input[5], c_input[0], c_input[1],
                    c_input[2]
                ]
            else:
                return c_input

        def test_branch_2(c_input, condition):
            if condition == False:
                self.assertGreaterEqual(c_input[3], 1, (2, c_input))
            else:
                self.assertLess(c_input[3], 1, (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

        def test_branch_3(c_input, condition):
            if condition == False:
                self.assertLessEqual(max(c_input[0], 1), 12, (3, c_input))
            else:
                self.assertGreater(max(c_input[0], 1), 12, (3, c_input))

        test_branch_3(result['3']['false']['vector'], False)
        test_branch_3(result['3']['true']['vector'], True)

        def test_branch_4(c_input, condition):
            if condition == False:
                self.assertLessEqual(max(c_input[3], 1), 12, (4, c_input))
            else:
                self.assertGreater(max(c_input[3], 1), 12, (4, c_input))

        test_branch_4(result['4']['false']['vector'], False)
        test_branch_4(result['4']['true']['vector'], True)

        def test_branch_5(c_input, condition):
            if condition == False:
                self.assertGreaterEqual(c_input[1], 1, (5, c_input))
            else:
                self.assertLess(c_input[1], 1, (5, c_input))

        test_branch_5(sanitize_month(result['5']['false']['vector']), False)
        test_branch_5(sanitize_month(result['5']['true']['vector']), True)

        def test_branch_6(c_input, condition):
            if condition == False:
                self.assertGreaterEqual(c_input[4], 1, (6, c_input))
            else:
                self.assertLess(c_input[4], 1, (6, c_input))

        test_branch_6(sanitize_month(result['6']['false']['vector']), False)
        test_branch_6(sanitize_month(result['6']['true']['vector']), True)

        def test_branch_7(c_input, condition):
            if condition == False:
                self.assertLessEqual(
                    max(c_input[1], 1),
                    month_days(c_input[0], c_input[2]), (7, c_input))
            else:
                self.assertGreater(
                    max(c_input[1], 1),
                    month_days(c_input[0], c_input[2]), (7, c_input))

        test_branch_7(sanitize_month(result['7']['false']['vector']), False)
        test_branch_7(sanitize_month(result['7']['true']['vector']), True)

        def test_branch_8(c_input, condition):
            if condition == False:
                self.assertLessEqual(
                    max(c_input[4], 1),
                    month_days(c_input[3], c_input[5]), (8, c_input))
            else:
                self.assertGreater(
                    max(c_input[4], 1),
                    month_days(c_input[3], c_input[5]), (8, c_input))

        test_branch_8(sanitize_month(result['8']['false']['vector']), False)
        test_branch_8(sanitize_month(result['8']['true']['vector']), True)

        def test_branch_9(c_input, condition):
            if condition == False:
                self.assertGreaterEqual(c_input[5], c_input[2], (9, c_input))
            else:
                self.assertLess(c_input[5], c_input[2], (9, c_input))

        test_branch_9(sanitize(result['9']['false']['vector']), False)
        test_branch_9(sanitize(result['9']['true']['vector']), True)

        def test_branch_10(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[5], c_input[2], (10, c_input))
            else:
                self.assertEqual(c_input[5], c_input[2], (10, c_input))

        test_branch_10(sanitize(result['10']['false']['vector']), False)
        test_branch_10(sanitize(result['10']['true']['vector']), True)

        def test_branch_11(c_input, condition):
            test_branch_10(c_input, True)
            if condition == False:
                self.assertGreaterEqual(c_input[3], c_input[0], (11, c_input))
            else:
                self.assertLess(c_input[3], c_input[0], (11, c_input))

        test_branch_11(sanitize(result['11']['false']['vector']), False)
        test_branch_11(sanitize(result['11']['true']['vector']), True)

        def test_branch_12(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[5], c_input[2], (12, c_input))
            else:
                self.assertEqual(c_input[5], c_input[2], (12, c_input))

        test_branch_12(sanitize(result['12']['false']['vector']), False)
        test_branch_12(sanitize(result['12']['true']['vector']), True)

        def test_branch_13(c_input, condition):
            test_branch_12(c_input, True)
            if condition == False:
                self.assertNotEqual(c_input[3], c_input[0], (13, c_input))
            else:
                self.assertEqual(c_input[3], c_input[0], (13, c_input))

        test_branch_13(sanitize(result['13']['false']['vector']), False)
        test_branch_13(sanitize(result['13']['true']['vector']), True)

        def test_branch_14(c_input, condition):
            test_branch_13(c_input, True)
            if condition == False:
                self.assertGreaterEqual(c_input[4], c_input[1], (14, c_input))
            else:
                self.assertLess(c_input[4], c_input[1], (14, c_input))

        test_branch_14(sanitize(result['14']['false']['vector']), False)
        test_branch_14(sanitize(result['14']['true']['vector']), True)

        def test_branch_15(c_input, condition):
            if condition == False:
                self.assertFalse(is_swap_needed(c_input), (15, c_input))
            else:
                self.assertTrue(is_swap_needed(c_input), (15, c_input))

        test_branch_15(sanitize(result['15']['false']['vector']), False)
        test_branch_15(sanitize(result['15']['true']['vector']), True)

        def test_branch_16(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0], c_input[3], (16, c_input))
            else:
                self.assertEqual(c_input[0], c_input[3], (16, c_input))

        test_branch_16(swap_dates(result['16']['false']['vector']), False)
        test_branch_16(swap_dates(result['16']['true']['vector']), True)

        def test_branch_17(c_input, condition):
            test_branch_16(c_input, True)
            if condition == False:
                self.assertNotEqual(c_input[2], c_input[5], (17, c_input))
            else:
                self.assertEqual(c_input[2], c_input[5], (17, c_input))

        test_branch_17(swap_dates(result['17']['false']['vector']), False)
        test_branch_17(swap_dates(result['17']['true']['vector']), True)

        def test_branch_18(c_input, condition):
            test_branch_16(c_input, False)
            if condition == False:
                self.assertNotEqual(c_input[2], c_input[5], (18, c_input))
            else:
                self.assertEqual(c_input[2], c_input[5], (18, c_input))

        test_branch_18(swap_dates(result['18']['false']['vector']), False)
        test_branch_18(swap_dates(result['18']['true']['vector']), True)

        def test_branch_19(c_input, condition):
            test_branch_18(c_input, True)

        test_branch_19(swap_dates(result['19']['false']['vector']), False)
        test_branch_19(swap_dates(result['19']['true']['vector']), True)

        def test_branch_20(c_input, condition):
            test_branch_18(c_input, False)

        test_branch_20(swap_dates(result['20']['false']['vector']), False)
        test_branch_20(swap_dates(result['20']['true']['vector']), True)

        def test_branch_21(c_input, condition):
            test_branch_18(c_input, False)

        test_branch_21(swap_dates(result['21']['false']['vector']), False)
        test_branch_21(swap_dates(result['21']['true']['vector']), True)

        def test_branch_22(c_input, condition):
            test_branch_18(c_input, False)

        test_branch_22(swap_dates(result['22']['false']['vector']), False)
        test_branch_22(swap_dates(result['22']['true']['vector']), True)

        def test_branch_23(c_input, condition):
            def is_leap_year_between(c_input, condition):
                for year in range(c_input[2] + 1, c_input[5]):
                    if is_leap_year(year) == condition:
                        return condition
                return not condition

            test_branch_22(c_input, True)
            if condition == False:
                self.assertFalse(
                    is_leap_year_between(c_input, condition), (23, c_input))
            else:
                self.assertTrue(
                    is_leap_year_between(c_input, condition), (23, c_input))

        test_branch_23(swap_dates(result['23']['false']['vector']), False)
        test_branch_23(swap_dates(result['23']['true']['vector']), True)

    def test_line(self):
        cmd = [
            'sample/line.c',
            '-f',
            'intersect',
            '--min',
            '0',
            '--termination',
            '100000',
            '--no-sandbox',
        ]
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 7)

        def extract(raw_c_input):
            return [raw_c_input[0]['members'], raw_c_input[1]['members']]

        def ua_t(c_input):
            return (c_input[1][2] - c_input[1][0]) * (
                c_input[0][1] - c_input[1][1]) - (
                    c_input[1][3] - c_input[1][1]) * (
                        c_input[0][0] - c_input[1][0])

        def ub_t(c_input):
            return (c_input[0][2] - c_input[0][0]) * (
                c_input[0][1] - c_input[1][1]) - (
                    c_input[0][3] - c_input[0][1]) * (
                        c_input[0][0] - c_input[1][0])

        def u_b(c_input):
            return (c_input[1][3] - c_input[1][1]) * (
                c_input[0][2] - c_input[0][0]) - (
                    c_input[1][2] - c_input[1][0]) * (
                        c_input[0][3] - c_input[0][1])

        def ua(c_input):
            return ua_t(c_input) / u_b(c_input)

        def ub(c_input):
            return ub_t(c_input) / u_b(c_input)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertEqual(u_b(c_input), 0, (1, c_input))
            else:
                self.assertNotEqual(u_b(c_input), 0, (1, c_input))

        test_branch_1(extract(result['1']['false']['vector']), False)
        test_branch_1(extract(result['1']['true']['vector']), True)

        def test_branch_2(c_input, condition):
            test_branch_1(c_input, True)
            if condition == False:
                self.assertGreaterEqual(0, ua(c_input), (2, c_input))
            else:
                self.assertLess(0, ua(c_input), (2, c_input))

        test_branch_2(extract(result['2']['false']['vector']), False)
        test_branch_2(extract(result['2']['true']['vector']), True)

        def test_branch_3(c_input, condition):
            test_branch_1(c_input, False)
            if condition == False:
                self.assertNotEqual(0, ua_t(c_input), (3, c_input))
            else:
                self.assertEqual(0, ua_t(c_input), (3, c_input))

        test_branch_3(extract(result['3']['false']['vector']), False)
        test_branch_3(extract(result['3']['true']['vector']), True)

        def test_branch_4(c_input, condition):
            test_branch_3(c_input, False)
            if condition == False:
                self.assertNotEqual(0, ub_t(c_input), (4, c_input))
            else:
                self.assertEqual(0, ub_t(c_input), (4, c_input))

        test_branch_4(extract(result['4']['false']['vector']), False)
        test_branch_4(extract(result['4']['true']['vector']), True)

        def test_branch_5(c_input, condition):
            test_branch_2(c_input, True)
            if condition == False:
                self.assertGreaterEqual(ua(c_input), 1, (5, c_input))
            else:
                self.assertLess(ua(c_input), 1, (5, c_input))

        test_branch_5(extract(result['5']['false']['vector']), False)
        test_branch_5(extract(result['5']['true']['vector']), True)

        def test_branch_6(c_input, condition):
            test_branch_5(c_input, True)
            if condition == False:
                self.assertGreaterEqual(0, ub(c_input), (6, c_input))
            else:
                self.assertLess(0, ub(c_input), (6, c_input))

        test_branch_6(extract(result['6']['false']['vector']), False)
        test_branch_6(extract(result['6']['true']['vector']), True)

        def test_branch_7(c_input, condition):
            test_branch_6(c_input, True)
            if condition == False:
                self.assertGreaterEqual(ub(c_input), 1, (7, c_input))
            else:
                self.assertLess(ub(c_input), 1, (7, c_input))

        test_branch_7(extract(result['7']['false']['vector']), False)
        test_branch_7(extract(result['7']['true']['vector']), True)

    def test_case1(self):
        cmd = ['sample/case1.c', '-f', 'case1']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 2)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertFalse(c_input[0] > 0 and c_input[1] > 0 and
                                 c_input[2] > 0, (1, c_input))
            else:
                self.assertTrue(c_input[0] > 0 and c_input[1] > 0 and
                                c_input[2] > 0, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def test_branch_2(c_input, condition):
            test_branch_1(c_input, True)
            if condition == False:
                self.assertNotEqual(c_input[0] + c_input[1],
                                    c_input[2] * c_input[2], (2, c_input))
            else:
                self.assertEqual(c_input[0] + c_input[1],
                                 c_input[2] * c_input[2], (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

    def test_case2(self):
        cmd = ['sample/case2.c', '-f', 'case2']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 2)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertFalse(
                    c_input[0]['pointee'] == None or
                    c_input[0]['pointee']['members'][0]['pointee'] == None, (
                        1, c_input))
            else:
                self.assertTrue(
                    c_input[0]['pointee'] == None or
                    c_input[0]['pointee']['members'][0]['pointee'] == None, (
                        1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

        def test_branch_2(c_input, condition):
            test_branch_1(c_input, False)
            if condition == False:
                self.assertFalse(c_input[0]['pointee']['members'][1] == 1 and
                                 c_input[0]['pointee']['members'][2] == 2, (
                                     2, c_input))
            else:
                self.assertTrue(c_input[0]['pointee']['members'][1] == 1 and
                                c_input[0]['pointee']['members'][2] == 2, (
                                    2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

    def test_case3(self):
        cmd = ['sample/case3.c', '-f', 'case3']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0], 10, (1, c_input))
            else:
                self.assertEqual(c_input[0], 10, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_case4(self):
        cmd = ['sample/case4.c', '-f', 'case4', '--min', '32', '--max', '127']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 2)

        def test_branch_2(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0]['pointee'],
                                    [72, 101, 108, 108, 111], (2, c_input))
            else:
                self.assertEqual(c_input[0]['pointee'],
                                 [72, 101, 108, 108, 111], (2, c_input))

        test_branch_2(result['2']['false']['vector'], False)
        test_branch_2(result['2']['true']['vector'], True)

    def test_case5(self):
        cmd = ['sample/case5.c', '-f', 'case5']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0], 10, (1, c_input))
            else:
                self.assertEqual(c_input[0], 10, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_case7(self):
        cmd = ['sample/case7.c', '-f', 'case7']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0]['pointee']['members'][3][
                    'pointee']['members'][1], 2, (1, c_input))
            else:
                self.assertEqual(c_input[0]['pointee']['members'][3]['pointee']
                                 ['members'][1], 2, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_pointer(self):
        cmd = ['sample/pointer.c', '-f', 'test']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0]['pointee']['members'][0] *
                                    c_input[0]['pointee']['members'][1],
                                    c_input[1]['pointee'], (1, c_input))
            else:
                self.assertEqual(c_input[0]['pointee']['members'][0] *
                                 c_input[0]['pointee']['members'][1],
                                 c_input[1]['pointee'][0], (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_intarr(self):
        cmd = ['sample/intarr.c', '-f', 'test']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0]['pointee'][1]
                                    if len(c_input[0]['pointee']) >= 2 else 0,
                                    100, (1, c_input))
            else:
                self.assertEqual(c_input[0]['pointee'][1], 100, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_chararr(self):
        cmd = ['sample/chararr.c', '-f', 'test']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0]['pointee'][1]
                                    if len(c_input[0]['pointee']) >= 2 else 0,
                                    100, (1, c_input))
            else:
                self.assertEqual(c_input[0]['pointee'][1], 100, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

    def test_char(self):
        cmd = ['sample/char.c', '-f', 'test']
        rc, stdout = run_cavm(cmd)
        self.assertEqual(rc, 0)
        result = json.loads(stdout.decode('utf-8'))
        self.assertion(result, 1)

        def test_branch_1(c_input, condition):
            if condition == False:
                self.assertNotEqual(c_input[0], 100, (1, c_input))
            else:
                self.assertEqual(c_input[0], 100, (1, c_input))

        test_branch_1(result['1']['false']['vector'], False)
        test_branch_1(result['1']['true']['vector'], True)

if __name__ == '__main__':
    unittest.main()
