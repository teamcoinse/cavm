int positive(int a) {
  if (a < -10) return 0;

  if (a++ > 0)
    if (++a == 10) return --a;
  return a--;
}
