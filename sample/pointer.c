struct Data {
  int a, b;
};

int test(struct Data *d, int *c) {
  if (d->a * d->b == *c) {
    return -1;
  } else {
    return 0;
  }
}
