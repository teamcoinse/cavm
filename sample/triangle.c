int fget_type(double a, double b, double c) {
  int type;

  if (a > b) {
    double t = a;
    a = b;
    b = t;
  }
  if (a > c) {
    double t = a;
    a = c;
    c = t;
  }
  if (b > c) {
    double t = b;
    b = c;
    c = t;
  }

  if (a + b <= c) {
    type = 0;
  } else {
    type = 1;
    if (a == b) {
      if (b == c) {
        type = 3;
      }
    } else {
      if (a == b) {
        type = 4;
      } else if (b == c) {
        type = 4;
      }
    }
  }

  return type;
}

int get_type(int a, int b, int c) {
  int type;

  if (a > b) {
    int t = a;
    a = b;
    b = t;
  }
  if (a > c) {
    int t = a;
    a = c;
    c = t;
  }
  if (b > c) {
    int t = b;
    b = c;
    c = t;
  }

  if (a + b <= c) {
    type = 0;
  } else {
    type = 1;
    if (a == b) {
      if (b == c) {
        type = 3;
      }
    } else {
      if (a == b) {
        type = 4;
      } else if (b == c) {
        type = 4;
      }
    }
  }

  return type;
}

int main(int argc, char* argv[]) {
  int a, b, c;
  a = 10;
  b = 3;
  c = 9;
  if (argc > 3)
    return get_type(a, b, c);
  else
    return 0;
}
