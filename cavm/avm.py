"""
Copyright (C) 2017 by Junhwi Kim <junhwi.kim23@gmail.com>
Copyright (C) 2017 by Byeonghyeon You <byou@kaist.ac.kr>

Licensed under the MIT License:
See the LICENSE file at the top-level directory of this distribution.
"""

"""avm
  pattern search method of avm
"""

import copy
import random

from cavm import ctype
from cavm import evaluation


class Individual:
    """input vector and its fitness value"""

    def __init__(self, vector, fitness):
        self.vector = vector
        self.fitness = fitness

    def get_vector(self):
        """return input vector"""
        return copy.deepcopy(self.vector)

    def get_fitness(self):
        """return fitness value"""
        return self.fitness


def local_search(obj_func, individual, current_elem, termination):
    if isinstance(current_elem, ctype.CType):
        return iterative_pattern_search(obj_func, individual, current_elem,
                                        termination)
    elif isinstance(current_elem, ctype.CStruct):
        for member in current_elem.members:
            fitness = local_search(obj_func, individual, member, termination)
        return fitness
    elif isinstance(current_elem, ctype.CPointer):
        if current_elem.pointee:
            if isinstance(current_elem.pointee, list):
                currfitness = individual.fitness
                for elem in current_elem.pointee:
                    fitness = local_search(obj_func, individual, elem,
                                           termination)
                if not fitness < currfitness:
                    newelem = ctype.make_CType(current_elem.underlying_type,
                                               obj_func.decls)
                    if isinstance(newelem, ctype.CType):
                        init_value([newelem])
                    current_elem.pointee.append(newelem)
                    individual.fitness = obj_func.get_fitness(
                        individual.vector)
                return individual.fitness

            else:
                return local_search(obj_func, individual, current_elem.pointee,
                                    termination)
        else:
            elem = ctype.make_CType(current_elem.underlying_type,
                                    obj_func.decls)
            if isinstance(elem, ctype.CType):
                init_value([elem])
                elem = [elem]
            current_elem.pointee = elem
            individual.fitness = obj_func.get_fitness(individual.vector)
            return individual.fitness


def vector_search(obj_func, individual, termination):
    """search each variable in input vector"""
    local_optimum = individual.fitness
    c_input = individual.vector

    non_improvement = 0
    while non_improvement < len(c_input):
        for elem in c_input:
            candidate = local_search(obj_func, individual, elem, termination)
            if candidate < local_optimum:
                local_optimum = candidate
                non_improvement = 0
            else:
                non_improvement += 1
    return local_optimum


def iterative_pattern_search(obj_func, init_input, current_elem, termination):
    """iterative pattern search"""
    scaling_factor = 2
    step = 0.1**current_elem._prec if current_elem.is_floating() else 1

    def pattern_search(individual, current_elem):
        """pattern search"""
        direction = search_direction(individual.fitness, individual.vector,
                                     current_elem)
        if direction == 0:
            return individual.fitness
        diff = direction * step
        prev_fitness = individual.fitness
        best = current_elem.value
        while obj_func.counter < termination:
            fitness = get_fitness_after_move(individual.vector, current_elem,
                                             diff)
            diff *= scaling_factor
            if fitness < prev_fitness:
                prev_fitness = fitness
                best = current_elem.value
            else:
                break
        current_elem.value = best
        individual.fitness = prev_fitness
        return prev_fitness

    def check_bound(var, diff, maximum, minimum):
        """check bound condition"""
        ret = var + diff
        if ret > maximum:
            ret = maximum
        elif ret < minimum:
            ret = minimum
        return ret

    def get_fitness_after_move(c_input, current_elem, size):
        value = check_bound(current_elem.value, size,
                            current_elem.get_max(), current_elem.get_min())
        if current_elem.is_floating():
            value = round(value, current_elem._prec)
        current_elem.value = value
        fitness = obj_func.get_fitness(c_input)
        return fitness

    def search_direction(fitness, c_input, current_elem):
        """provide search direction"""
        original_value = current_elem.value
        current_fitness = fitness
        left_fitness = get_fitness_after_move(c_input, current_elem, -step)
        current_elem.value = original_value
        right_fitness = get_fitness_after_move(c_input, current_elem, step)
        current_elem.value = original_value

        if right_fitness < current_fitness:
            return 1
        elif left_fitness < current_fitness:
            return -1
        else:
            return 0

    local_optimum = init_input.fitness
    best = current_elem.value
    while obj_func.counter < termination:
        candidate = pattern_search(init_input, current_elem)
        if candidate < local_optimum:
            local_optimum = candidate
            best = current_elem.value
        else:
            break
    current_elem.value = best
    init_input.fitness = local_optimum
    return local_optimum


def init_value(params):
    """initialize random input value"""
    for param in params:
        if isinstance(param, ctype.CStruct):
            init_value(param.members)
        elif isinstance(param, ctype.CType):
            if param.is_integer():
                var = random.randint(param.get_min(), param.get_max())
            elif param.is_floating():
                var = round(
                    random.uniform(param.get_min(), param.get_max()),
                    param._prec)
            else:
                raise NotImplementedError
            param.value = var


def search(obj_func, params, branchlist, termination):
    """repeat avm for every target branch in branchlist"""
    ret = []
    original = params
    covered = []
    for target in branchlist:
        if target in covered:
            continue
        params = copy.deepcopy(original)
        obj_func.set_target(target)
        while obj_func.counter < termination:
            init_value(params)
            try:
                initial_individual = Individual(params,
                                                obj_func.get_fitness(params))
                result = vector_search(obj_func, initial_individual,
                                       termination)
            except evaluation.GlobalOptima:
                ret.append((target, copy.deepcopy(params), obj_func.counter))
                covered.append(target)
                break
        else:
            ret.append((target, None, termination))
            covered.append(target)
        trace = obj_func.execute(params)
        new_covered = [
            branch for branch in evaluation.get_covered_branches(trace)
            if branch not in covered
        ]
        for branch in new_covered:
            ret.append((branch, copy.deepcopy(params), 0))
            covered.append(branch)

    return ret
