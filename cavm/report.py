"""
Copyright (C) 2017 by Junhwi Kim <junhwi.kim23@gmail.com>
Copyright (C) 2017 by Byeonghyeon You <byou@kaist.ac.kr>
Copyright (C) 2017 by Gabin An <agb94@kaist.ac.kr>

Licensed under the MIT License:
See the LICENSE file at the top-level directory of this distribution.
"""

"""report
  report result of input generation
"""

import csv
import json

from cavm import ctype
from cavm import evaluation

class CTypeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ctype.CType):
            return obj.value
        elif isinstance(obj, ctype.CStruct):
            return {
                'name': obj.name,
                'is_recursive': obj.is_recursive,
                'members': obj.members,
                'decl': obj.decl
            }
        elif isinstance(obj, ctype.CPointer):
            return {
                'underlying_type': obj.underlying_type,
                'pointee': obj.pointee,
            }
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


def make_JSON(result):
    """convert serach result into json"""
    data = {}
    total = 0
    covered = 0
    
    for target, vector, cnt in result:
        if not target[0] in data:
            data[target[0]] = {}
        data[target[0]][target[1]] = {
            'vector': [elem for elem in vector] if vector else vector,
            'counter': cnt
        }
        if vector:
            covered = covered + 1
        total = total + cnt
    data['report'] = {
      'totalEvals': total,
      'coveredBranches': covered,
      'totalBranches': len(result)
    }
    return json.dumps(data, indent=2, cls=CTypeEncoder)


def make_csv(result, name):
    """Make coverage report in json form"""
    with open("branch_coverage_%s.csv" % name, 'w', newline='') as csvfile:
        w = csv.writer(csvfile)
        w.writerow(['node_id', 'condition', 'evaluations', 'covered'])
        for target, vector, cnt in result:
            row = []
            row.append(target[0])
            row.append(target[1])
            row.append(cnt)
            row.append(vector != None)
            w.writerow(row)


def coverage(ffi, dlib, target_function, result):
    """Execute binary with gcov option"""
    obj = evaluation.ObjFunc(target_function, dlib, ffi, None, None, None, None, True)
    for target, vector, cnt in result:
        if vector:
          obj.execute(vector, True)
