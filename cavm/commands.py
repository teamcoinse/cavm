"""
Copyright (C) 2017 by Junhwi Kim <junhwi.kim23@gmail.com>

Licensed under the MIT License:
See the LICENSE file at the top-level directory of this distribution.
"""

"""CAVM commands
  Commands for CAVM
"""

import argparse


class BaseCommand:
    def execute(self):
        """
      Execute this command.
      """
        pass

    def add_args(self, parser):
        """
      Add custom arguments for command.
      """
        parser.add_argument(
            'target',
            metavar='<target file>',
            type=str,
            help='location of target c file')
        parser.add_argument(
            '-f',
            '--function',
            metavar='<target function>',
            type=str,
            help='name of function',
            required=False)
        parser.add_argument(
            '--flags',
            nargs=argparse.REMAINDER,
            default=[],
            help='custom flags for compilation')


class Instrument(BaseCommand):

    description = 'Instrument a given c function'


class Run(Instrument):

    description = 'Run CAVM without own build chain'

    def add_args(self, parser):
        super().add_args(parser)
        parser.add_argument(
            '-b',
            '--branch',
            metavar='<target branch>',
            type=str,
            help='name of branch',
            required=False)
        parser.add_argument(
            '--min',
            metavar='<minimum random value>',
            type=int,
            help='random minimum',
            default=-100,
            required=False)
        parser.add_argument(
            '--max',
            metavar='<maximum random value>',
            type=int,
            help='random maximum',
            default=100,
            required=False)
        parser.add_argument(
            '--termination',
            metavar='<search iteration bound>',
            type=int,
            help='iteration bound',
            default=1000,
            required=False)
        parser.add_argument(
            '--prec',
            metavar='<precision>',
            type=int,
            help='precision of floating numbers',
            required=False)
        parser.add_argument(
            '--no-sandbox',
            dest='sandbox',
            action='store_false',
            help='Don\'t use sandbox',
            default=True,
            required=False)
        parser.add_argument(
            '--gcov',
            dest='gcov',
            action='store_true',
            help='Use gcov',
            default=False,
            required=False)
        parser.add_argument(
            '--coverage',
            dest='coverage',
            action='store_true',
            help='Make coverage report in csv',
            default=False,
            required=False)


class Search(Run):

    description = 'Do AVM search over a given c function'

    def execute(self):
        pass

    def add_args(self, parser):
        super().add_args(parser)
        parser.add_argument(
            'binary',
            metavar='<binary>',
            type=str,
            help='location of chared library binary')
